﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;

namespace LibraryManagement.Models
{
    

    public class LibraryManagementDbContext : DbContext
    {
        public LibraryManagementDbContext()
            : base("DefaultConnection")
        {
        }

        public static LibraryManagementDbContext Create()
        {
            return new LibraryManagementDbContext();
        }
        public DbSet<Book> Books { get; set; }
        public DbSet<Member> Members { get; set; }
        public DbSet<Librarian> Librarians { get; set; }
        public DbSet<BorrowHistory> BorrowHistories { get; set; }
        public DbSet<Reservation> Reservations { get; set; }
    }
}