﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ADO_NET_Example
{
    class Program
    {
        static void Main(string[] args)
        {
            string connectionString = "Data Source = (Local); Initial Catalog = DB_University; Integrated Security=true";

            string queryString = "SELECT * from student";
            ShowStudentData(connectionString, queryString);
            queryString = "update student set name= 'Francois' where Id =2";
            using (SqlConnection connnection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connnection);
                command.Connection.Open();
                int rows_effected = command.ExecuteNonQuery();
                Console.WriteLine(rows_effected + "rows effected");
            }

            ShowStudentData(connectionString, queryString);
            Console.ReadLine();

        }

        private static void ShowStudentData(string connectionString, string queryString)
        {
           using (SqlConnection connection = new SqlConnection(connectionString))
            {
                SqlCommand command = new SqlCommand(queryString, connection);
                try
                {
                    command.Connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        Console.WriteLine("\t{0}\t{1}\t{2}", reader[0], reader[1], reader[2]);
                    }
                    reader.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
                Console.ReadLine();
            }
        }
    }
}
