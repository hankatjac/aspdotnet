﻿using EF_DatabaseFirst.DataBase;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_DatabaseFirst
{
    class Program
    {
        static void Main(string[] args)
        {
            using (DB_UniversityEntities context = new DB_UniversityEntities())
            {
                //LINQ: language Integrated Query
          

                //add object to db
                context.Cities.Add(new City { Name = "Frankfurt" });
                context.SaveChanges();

                //remove
                var a = context.Cities.FirstOrDefault(x => x.Name == "Aachen" );
                if (a != null)
                {
                    context.Cities.Remove(a);
                    context.SaveChanges();

                }

                //edit
                var a_city = context.Cities.FirstOrDefault(x => x.Name == "Toronto");
                if (a_city != null)
                {
                    a_city.Name = "Vancouver";
                    context.SaveChanges();
                }

                


                var city = context.Cities.FirstOrDefault(x => x.Id == 1);
                Console.WriteLine("City name is : " + city.Name);
                Console.ReadLine();

                var cities = context.Cities.Where(x => x.Name.StartsWith("Mo")).OrderByDescending(x => x.Id).ToList();
                foreach (City _city in cities)
                {
                    Console.WriteLine(_city.Name);
                }

                Console.ReadLine();

            }
        }
    }
}
