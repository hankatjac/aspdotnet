﻿using System.Web.Mvc;

namespace ControllersAndActions.Controllers
{
    public class myCustomeRedirectResult : ActionResult
    {
        public string Url { get; set; }

        public override void ExecuteResult(ControllerContext context)
        {
            string fullURL = UrlHelper.GenerateContentUrl(Url, context.HttpContext);
            context.HttpContext.Response.Redirect(fullURL);
        }
    }
}