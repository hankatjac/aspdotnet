﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home



        public String MyAction()
        {
            return "Hello from Controller, Welcome shaghayegh to first MVC application";
        }


        public ViewResult Index()

        {
            int hour = DateTime.Now.Hour;
            ViewBag.Greeting = hour < 12 ? "Good Morning" : "Good Afternoon";

            ViewBag.Message = "Hello";
            ViewBag.Date = DateTime.Now;
            return View();
        }


        public ActionResult SecondView()
        {
            Random rand = new Random();
            int nextRandomNumber = rand.Next(1,1000);

            ViewBag.Message = nextRandomNumber;


            return View();

        }
    }
}