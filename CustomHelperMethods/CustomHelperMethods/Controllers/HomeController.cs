﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CustomHelperMethods.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.fruits = new string[] { "Apple", "Orange", "Pear" };
            ViewBag.cities = new string[] { "Montreal", "Sherbrook", "Quebec", "Laval" };
            return View();
        }
    }
}