﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace PersonMVC.Models
{
    public class Person
    {
        public string Name { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string Gender { get; set; }
        public string Age { get; set; }
        public string Address { get; set; }
    }

    public enum Gender
    {
        Male,
        Female
    }
}