﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AjaxWithJquery.Models
{
    public class PersonModel
    {
        public List<RoleModel> Roles { get; set; }
        public string Name { get; set; }
    }
}