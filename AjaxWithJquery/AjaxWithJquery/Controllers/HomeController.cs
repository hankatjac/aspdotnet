﻿using AjaxWithJquery.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AjaxWithJquery.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }

        public string Hello()
        {
            return @"<b>This is a bold text</b>
                     <br/>
                     <p>This is a paragraph</p>";
        }


        [HttpPost]
        public string PostMethodForAjax(string name, string city)
        {
            return name + "is from " + city;
        }


        [HttpPost]
        public ActionResult AddUser(PersonModel model)
        {

            if(model != null)
            {
                return Json("Success");
            }
            else
            {
                return Json("An error has occoured");
            }
            
        }




    }

    
}