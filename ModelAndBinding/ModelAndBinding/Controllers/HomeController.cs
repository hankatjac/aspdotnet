﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ModelAndBinding.Models
{
    public class HomeController : Controller
    {
        private Person[] personData =
        {
            new Person{PersonId=1, FirstName="John", LastName="Abbott", Role=Role.Admin},
            new Person{PersonId=2, FirstName="Isaac", LastName="Newton", Role=Role.User},
            new Person{PersonId=3, FirstName="Bill", LastName="Gates", Role=Role.User},
            new Person{PersonId=4, FirstName="Nelson", LastName="Mandela", Role=Role.Guest}
        };
        //
        // GET: /Home/
            public ActionResult Index(int id=1)
        {
            Person dataItem = personData.Where(p => p.PersonId == id).First();
            return View(dataItem);
        }

        public ActionResult CreatePerson()
        {
            return View(new Person());
        }

        [HttpPost]
        public ActionResult CreatePerson(Person model)
        {
            //process model
            return View("Index", model);
        }

        public ActionResult DisplaySummary([Bind(Prefix ="HomeAddress")]AddressSummary summary)
        {
            return View(summary);
        }


        public ActionResult Names(string[] names)
        {
            names = names ?? new string[0];
            return View(names);
        }

    
        public ActionResult Address (IList<AddressSummary> addresses)
        {
            addresses = addresses ?? new List<AddressSummary>();
            return View(addresses);
        }



    }
}