﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ModelAndBinding.Models
{
    public class MustBeTrueAttribute : ValidationAttribute
    {
        public override bool IsValid(object value)
        {
            return value is bool && (bool)value;
        }
    }
}