﻿using System;
using System.ComponentModel.DataAnnotations;

namespace ModelAndBinding.Models
{
    public class NoJohnMondaysAttribute : ValidationAttribute
    {
        public NoJohnMondaysAttribute()
        {
            ErrorMessage = "John cannot boot appointments on Mondays";
        }

        public override bool IsValid(object value)
        {
            Appointment app = value as Appointment;
            if (app ==null || string.IsNullOrEmpty(app.ClientName) ||app.Date == null)
            {
                return true;
            }
            else
            {
                return !(app.ClientName.ToLower() == "John" && app.Date.DayOfWeek == DayOfWeek.Monday);
            }
           
        }
    }
}