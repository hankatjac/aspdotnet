﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using LibraryManagement.Models;
using System.Web.Security;

namespace LibraryManagement.Controllers
{
    
    public class AccountController : Controller
    {

        private LibraryManagementDbContext db = new LibraryManagementDbContext();
    
        // GET: /Account/Login
        [AllowAnonymous]
        public ActionResult Login()
        {
            //ViewBag.ReturnUrl = returnUrl;
            return View();
        }

   
        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Login(Librarian librarian)
        {
            if (!ModelState.IsValid)
            {
                var user = db.Librarians.FirstOrDefault(u => u.Email == librarian.Email && u.Password == librarian.Password);
                if (user != null)
                {
                    //Session["UserID"] = user.LibrarianId.ToString();
                    //Session["Username"] = user.Name.ToString();
                    FormsAuthentication.SetAuthCookie(user.Name, false);
                    return RedirectToAction("Index", "Home");
                }
                else
                {
                    ModelState.AddModelError("", "email or Password is wrong.");
                }
                
            }
            return View();
        }


        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Login");
            //return RedirectToAction("Index", "Home");
        }


        //
        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        //
        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult Register(Librarian librarian)
        {
            bool IsMemberEmailExist = db.Librarians.Any(x => x.Email == librarian.Email);
            if (IsMemberEmailExist == true)
            {
                ModelState.AddModelError("Email", "Email already exists");
            }

            if (ModelState.IsValid)
            {
                db.Librarians.Add(librarian);
                db.SaveChanges();
                return RedirectToAction("Login");
            }

            return View(librarian);

        }

 

        

       
        

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

       
    }
}