﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace LibraryManagement.Models
{
    public class Member
    {
        public int MemberId { get; set; }

        [Required]
        [Display(Name = "Member Name")]
        public string Name { get; set; }

        [Required]
        public string Address { get; set; }

        [Required]
        [RegularExpression("\\d{3}-\\d{3}-\\d{4}", ErrorMessage = "Invalid phone number")]
        public string Contact { get; set; }

        [Required]
        [EmailAddress]
        public string Email { get; set; }

    }
}