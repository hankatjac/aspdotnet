﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http;

namespace WebAPI_SelfHosting.Controller
{
    public class MyController: ApiController
    {
        [HttpGet]
        public string Hello()
        {
            return "hello world from web api self hosted!";
        }
    }
}
