﻿using ABCOnlineBookingSystem.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABCOnlineBookingSystem.Controllers
{
    public class BookingController : Controller
    {
        // GET: Booking
     
        public ViewResult MakeBooking()
        {
            return View(new Appointment { Date = new DateTime(2019, 1, 1, 0, 0, 0) });
        }

        [HttpPost]
        public ViewResult MakeBooking(Appointment appt)
        {
            if (ModelState.IsValid)
            {
                // save to data base
                return View("Completed", appt);
            }
            else
            {
                return View();
            }
        }
    }
}