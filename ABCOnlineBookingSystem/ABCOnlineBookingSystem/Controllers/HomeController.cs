﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ABCOnlineBookingSystem.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            return View();
        }



        [HttpGet]
        public FileResult DownloadFile()
        {
            return new FilePathResult(@"~\Downloads\Schedule.pdf", "application/pdf");
        }

        public FileResult DownloadImage()
        {
            return new FilePathResult(@"~\Downloads\Customer-Thinking.jpg", "image/jpeg");
        }


    


    }
}