﻿using ABCOnlineBookingSystem.Infrastructure;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ABCOnlineBookingSystem.Models
{
    [NoJackOnFridaysAttribute]
    [NoJohnOnMondaysAttribute]
    
    //model validation attribute
    //must check ModelState.IsValid
    public class Appointment
    {
        [Required]
        public string ClientName { get; set; }

        [Required(ErrorMessage = "Please enter a date")]
        [FutureDate(ErrorMessage = "Please enter a date in the future and not in weekend")]
        public DateTime Date { get; set; }

        [MustBeTrue(ErrorMessage = "You must accept the terms")]
        public bool TermsAccepted { get; set; }
    }
}