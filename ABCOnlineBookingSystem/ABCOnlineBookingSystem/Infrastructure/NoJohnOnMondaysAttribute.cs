﻿using ABCOnlineBookingSystem.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ABCOnlineBookingSystem.Infrastructure
{
    public class NoJohnOnMondaysAttribute : ValidationAttribute
    {
        public NoJohnOnMondaysAttribute()
        {
            ErrorMessage = "John cannot book appointments on Mondays";         
        }
        public override bool IsValid(object value)
        {
            Appointment app = value as Appointment;
            if (app == null || string.IsNullOrEmpty(app.ClientName) || app.Date == null)
            {
                return true;
            }
            else
            {
                return !(app.ClientName.ToLower() == "john" && app.Date.DayOfWeek == DayOfWeek.Monday);
            }
        }
    }
}