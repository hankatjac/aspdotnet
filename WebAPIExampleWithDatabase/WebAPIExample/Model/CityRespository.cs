﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebAPIExample.Model;

namespace WebAPIExample.Database {
    public class CityRepository
    {
        private static CityRepository repo = new CityRepository();

        public static CityRepository Current
        {
            get {return repo;}
        }

         public IEnumerable<City> GetAll()
        {
            using (DB_UniversityEntities ctx = new DB_UniversityEntities())
            {
                return ctx.Cities.ToList();
            }
     
        }
    
        public City Get(int id)
        {
            using (DB_UniversityEntities ctx = new DB_UniversityEntities())
            {
                return ctx.Cities.FirstOrDefault(x => x.Id == id);
            }
               
        }

        public City Add(City item)
        {
            using (DB_UniversityEntities ctx = new DB_UniversityEntities())
            {
                ctx.Cities.Add(item);
                ctx.SaveChanges();
                return item;
            }
          
        }

        public void Remove(int id)
        {
            using (DB_UniversityEntities ctx = new DB_UniversityEntities())
            {
                var item = ctx.Cities.FirstOrDefault(x => x.Id == id);
                ctx.Cities.Remove(item);
                ctx.SaveChanges();
            }   
        }

        public bool Update(City item)
        {
            City storedItem = Get(item.Id);
            if (storedItem != null)
            {
                storedItem.Name = item.Name;
               
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
