﻿using System.Web.Mvc;
using WebAPIExample.Database;
using WebAPIExample.Model;

namespace WebAPIExample.Controllers {

    public class HomeController : Controller
    {
        private CityRepository repo = CityRepository.Current;

        public ViewResult Index()
        {
            return View(repo.GetAll());
        }

        public ActionResult Add(City item)
        {
            if (ModelState.IsValid)
            {
                repo.Add(item);
                return RedirectToAction("Index");
            }
            else
            {
                return View("Index");
            }
        }


        public ActionResult Remove(int id)
        {
            repo.Remove(id);
            return RedirectToAction("Index");
        }


        public ActionResult Update(City item)
        {
            if(ModelState.IsValid && repo.Update(item))
            {
                return RedirectToAction("Index");
            }
            else
            {
                return View("Index");
            }
        }
    }
}
