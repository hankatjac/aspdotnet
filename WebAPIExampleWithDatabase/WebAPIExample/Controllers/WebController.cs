﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebAPIExample.Database;
using WebAPIExample.Model;

namespace WebAPIExample.Controllers
{

    public class WebController : ApiController
    {
        private CityRepository repo = CityRepository.Current;

        public IEnumerable<City> GetAllCities()
        {
            return repo.GetAll();
        }

        [HttpGet]
        public City GetCity(int id)
        {
            return repo.Get(id);
        }

        [HttpPost]
        public City PostCity(City item)
        {
            return repo.Add(item);
        }

        [HttpPut]
        public bool PutCity(City item)
        {
            return repo.Update(item);
        }

        public void DeleteCity(int id)
        {
            repo.Remove(id);
        }
    }
}
