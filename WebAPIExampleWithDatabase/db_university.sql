create database DB_University;
go

use DB_University;
go


create table City
(
	Id int not null identity (1, 1),
	Name varchar(50),
	-- constraint constraint_name constraint_type
	constraint pk_City primary key clustered (Id)
);
go

create table Course
(
	Id int not null identity (1, 1),
	Name varchar(50),
	Credit int not null 
	-- constraint constraint_name constraint_type
	constraint pk_Course primary key clustered (Id)
);
go


create table CourseStudent
(
	studentId int not null ,
	courseId int not null,
	term nvarchar(30) not null,
	
	-- constraint constraint_name constraint_type
	constraint pk_CourseStudent primary key clustered (studentID, courseId, term)
	
);
go

create table Student
(
	Id int not null identity (1, 1),
	Name nvarchar(40) null,
	Faimily nvarchar(30) null,
	Avg float not null,
	Age int not null,
	CityId int null

	-- constraint constraint_name constraint_type
	constraint pk_Student primary key clustered (Id)
);
go


alter table Student
	add constraint fk_Student_City foreign key (CityId)
	references  City (Id);
go

alter table CourseStudent
	add constraint fk_CourseStudent_Student foreign key (studentId)
	references Student (Id);
go

alter table CourseStudent
	add constraint fk_CourseStudent_Course foreign key (courseId)
	references Course (Id);
go


insert into City
values ('Frankfurt'),('Toronto');
go



insert into Student
values ('Albert', 'Ienstein', 90, 25, 1);
go

insert into Student
values ('Issac', 'Newton', 96, 30, 2 );
go

SELECT TOP (1000) [Id]
      ,[Name]
      ,[Faimily]
      ,[Avg]
      ,[Age]
  FROM [DB_University].[dbo].[Student]


SELECT * FROM City
go
