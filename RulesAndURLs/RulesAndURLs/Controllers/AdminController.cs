﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RulesAndURLs.Controllers
{
    public class AdminController : Controller
    {
        // GET: Admin
        public ActionResult Index()
        {
            ViewBag.controller = "Admin";
            ViewBag.action = "index";
            return View("ActionName");
        }

        public ActionResult ChangePassword()
        {
            ViewBag.controller = "Admin";
            ViewBag.action = "ChangePassword";
            return View("ActionName");
        }

    }
}