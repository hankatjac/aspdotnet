﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace RulesAndURLs.Controllers
{
    public class HomeController : Controller
    {
        // GET: Home
        public ActionResult Index()
        {
            ViewBag.controller= "Home";
            ViewBag.action = "index";
            return View("ActionName");
        }

        public ActionResult ChangePassword()
        {
            ViewBag.controller = "Home";
            ViewBag.action = "ChangePassword";
            return View("ActionName");
        }

        public ActionResult CustomeVariable()
        {
            ViewBag.controller = "Home";
            ViewBag.action = "CustomeVariable";
            ViewBag.CustomeVariable = RouteData.Values["id"];

            return View("ActionName");
        }

        public ActionResult CustomeVariableWithParameter(string id)
        {
            ViewBag.controller = "Home";
            ViewBag.action = "CustomeVariable";
            ViewBag.CustomeVariable = id??"<no value>";

            return View("ActionName");
        }
    }
}