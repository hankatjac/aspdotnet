namespace EF_CodeFirst.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_NationalCode_to_Student : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Students", "NationalCode", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Students", "NationalCode");
        }
    }
}
