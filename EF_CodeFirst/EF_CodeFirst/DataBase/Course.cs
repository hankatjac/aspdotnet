﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_CodeFirst.DataBase
{
    public class Course
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public int Unit { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }
    }
}
