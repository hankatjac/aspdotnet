﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EF_CodeFirst.DataBase
{
    public class Student
    {
        public int Id { get; set; }

        public string Name { get; set; }

        public string Family { get; set; }

        public string NationalCode { get; set; }

        public virtual ICollection<Enrollment> Enrollments { get; set; }

    }
}
